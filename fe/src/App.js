import { useState, useEffect } from "react";
import io from "socket.io-client";
import './App.css';

const socket = io("http://localhost:3000/");

function App() {
  const [count, setCount] = useState(0);

  // get the current count value
  useEffect(() => {
    socket.on("currentCount", (currentCount) => {
      setCount(currentCount)
    });

    return () => {
      socket.disconnect();
    }
  }, [])

  const addOne = () => {
    socket.emit("increaseCount");
  }

  return (
    <div className="App">
      <h1>The current count is: {count}</h1>
      <button onClick={addOne}>+ 1</button>
    </div>
  );
}

export default App;
