const { createServer } = require("http");
const { Server } = require("socket.io");

// You need to worry about are problems arising when multiple users use the site at the same time
let count = 0;

const httpServer = createServer();

const io = new Server(httpServer, {
	cors: {
		origin: "http://localhost:3000/",
	}
});

io.on("connection", (socket) => {

	socket.broadcast.emit("currentCount", count);

	socket.on("increaseCount", () => {
		count + 1;

		io.broadcast.emit("currentCount", count);
	});
});

httpServer.listen(3000, () => {
	console.log("server is up!")
});